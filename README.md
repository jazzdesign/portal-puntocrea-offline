**Portal Punto Crea - Offline**

Esta plataforma esta realizada con el framework Symfony Version 3.4

Documentación de Symfony 3.4
https://symfony.com/doc/master/index.html#gsc.tab=0

---

## Requerimientos

Estos son los requerimientos necesarios para poder hacer la instalación en los CAPs.

1. PHP 5.5 o nueva versión
2. Mysql
3. Composer

Para instalar composer> https://getcomposer.org/download/

---

## Base de Datos

La base de datos debe llamarse portal_ejc_v3
usuario: root
password: $ejc1820%

---

## Instalación

Para realizar la instalación:

1. Descargar toda la carpeta /portal-puntocrea-offline de este repositorio.
2. Crear base de datos llamada portal_ejc_v3 | restablecer la base de datos desde el archivo portal-dump.sql que esta en este repositorio en este link puedes encontrar ese archivo: https://drive.google.com/drive/folders/1z_L2SGVUSeJDEuBy311VBzDWJvUDbHsy?usp=sharing.
3. Checkear que este instalado composer.
4. Insertar carpeta de repositorio dentro del CAP | configurar virtualhost a que apunte a la carpeta /portal-punto-crea-offline/web/
5. Correr los siguientes comandos:
	5.1 Borrar composer.lock, /vendor
	5.2 sudo composer install
	5.3 Borrar composer.lock, /vendor
	5.4 sudo SYMFONY_ENV=prod composer install --no-dev --optimize-autoloader
	5.5 sudo php bin/console cache:clear --env=prod --no-debug


---
