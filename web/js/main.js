// Borrar Tabla

const retos = document.getElementById('retos');

// if(retos){
//   retos.addEventListener('click', (e) => {
//     if(e.target.className === 'btn btn-danger delete-reto'){
//       if(confirm('Esta seguro que desea borrar reto?')){
//         const id = e.target.getAttribute('data-id');
//         console.log(id);
//         fetch(`/retos/borrar/${id}`, {
//           method: 'DELETE'
//         }).then(res => window.location.reload());
//       }
//     }
//   });
// }


//Definimos los settings necesarios para el editor.
let settings = {
  selector: 'textarea.editor',
  menubar: false,
  height: 600,
  theme: 'modern',
  plugins: 'image code print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help table',
  toolbar1: 'undo redo | link image | code | formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | table',
  image_advtab: true,
  image_title: true,
  // enable automatic uploads of images represented by blob or data URIs
  automatic_uploads: true,
  // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
  // images_upload_url: 'postAcceptor.php',
  // here we add custom filepicker only to Image dialog
  file_picker_types: 'image',
  // and here's our custom image picker
  file_picker_callback: function (cb, value, meta) {
    var input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');

    // Note: In modern browsers input[type="file"] is functional without 
    // even adding it to the DOM, but that might not be the case in some older
    // or quirky browsers like IE, so you might want to add it to the DOM
    // just in case, and visually hide it. And do not forget do remove it
    // once you do not need it anymore.

    input.onchange = function () {
      var file = this.files[0];

      var reader = new FileReader();
      reader.onload = function () {
        // Note: Now we need to register the blob in TinyMCEs image blob
        // registry. In the next release this part hopefully won't be
        // necessary, as we are looking to handle it internally.
        var id = 'blobid' + (new Date()).getTime();
        var blobCache = tinymce.activeEditor.editorUpload.blobCache;
        var base64 = reader.result.split(',')[1];
        var blobInfo = blobCache.create(id, file, base64);
        blobCache.add(blobInfo);

        // call the callback and populate the Title field with the file name
        cb(blobInfo.blobUri(), { title: file.name });
      };
      reader.readAsDataURL(file);
    };

    input.click();
  }

}
tinymce.init(settings);


$("#set-data-btn").on("click", function (e) {
  let contentSituacion = $("#fieldSituacion").text();
  tinymce.get("editorSituacion").setContent(contentSituacion);

  let contentPregunta = $("#fieldPregunta").text();
  tinymce.get("editorPregunta").setContent(contentPregunta);
  
  let contentInvestigo = $("#fieldInvestigo").text();
  tinymce.get("editorInvestigo").setContent(contentInvestigo);
  
  let contentMateriales = $("#fieldMateriales").text();
  tinymce.get("editorMateriales").setContent(contentMateriales);
  
  let contentEnsayo = $("#fieldEnsayo").text();
  tinymce.get("editorEnsayo").setContent(contentEnsayo);
  
  let contentConocer = $("#fieldConocer").text();
  tinymce.get("editorConocer").setContent(contentConocer);
  
  let contentAplicar = $("#fieldAplicar").text();
  tinymce.get("editorAplicar").setContent(contentAplicar);


  $('#edit-disclaimer').hide();
  $('#view-container').hide();
  $('#edit-container').show();

});



$(document).ready(function() {

  //Formulario de Retos Nuevos
  $('#form-new-reto').submit(function(e){
    //Se obtienen los contenidos de los wsywg
    let editorSituacion = tinymce.get("editorSituacion").getContent();
    let editorPregunta = tinymce.get("editorPregunta").getContent();
    let editorInvestigo = tinymce.get("editorInvestigo").getContent();
    let editorMateriales = tinymce.get("editorMateriales").getContent();
    let editorEnsayo = tinymce.get("editorEnsayo").getContent();
    let editorConocer = tinymce.get("editorConocer").getContent();
    let editorAplicar = tinymce.get("editorAplicar").getContent();
    $('#fieldSituacion').html(editorSituacion);
    $('#fieldPregunta').html(editorPregunta);
    $('#fieldInvestigo').html(editorInvestigo);
    $('#fieldMateriales').html(editorMateriales);
    $('#fieldEnsayo').html(editorEnsayo);
    $('#fieldConocer').html(editorConocer);
    $('#fieldAplicar').html(editorAplicar);

  });

//  $('#form-new-pagina').submit(function(e){
//     console.log('este es');
//  });

  $("#page-data-btn").on("click", function (e) {
    let contentSituacion = $("#fieldSituacion").text();
    tinymce.get("editorSituacion").setContent(contentSituacion); 
    
    $('#edit-disclaimer').hide();
    $('#edit-container').show();

  });


  $('#form-new-page').submit(function(e){
    let editorSituacion = tinymce.get("editorSituacion").getContent();
    $('#fieldSituacion').html(editorSituacion);
  });


  //Datatable for List Retos

  $('#listReto').DataTable({
    responsive: true,
    "order": [[0, "desc"]],
    "language": {
      "lengthMenu": "Mostrar _MENU_ resultados por página",
      "zeroRecords": "No encontramos nada - lo sentimos",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoEmpty": "No datos disponibles",
      "infoFiltered": "(filtrado de _MAX_ total de resultados)",
      "search" : "Buscar",
      "paginate": {
        "previous": "Anterior",
        "next": "Siguiente"
      }
    }
  });

 
});


