<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Competencias
 *
 * @ORM\Table(name="competencias")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompetenciasRepository")
 */
class Competencias
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="parentid", type="integer", nullable=true)
     */
    private $parentid;

    /**
     * @var string
     *
     * @ORM\Column(name="fuente", type="string", length=255)
     */
    private $fuente;

    /**
     * @var array
     *
     * @ORM\Column(name="area", type="array")
     */
    private $area;

    /**
     * @var string
     *
     * @ORM\Column(name="areaTitle", type="string", length=255)
     */
    private $areaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="subArea", type="string", length=255)
     */
    private $subArea;

    /**
     * @var string
     *
     * @ORM\Column(name="Nivel", type="string", length=255)
     */
    private $nivel;

    /**
     * @var int
     *
     * @ORM\Column(name="etapa", type="integer")
     */
    private $etapa;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text")
     */
    private $contenido;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=255)
     */
    private $tipo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parentid
     *
     * @param integer $parentid
     *
     * @return Competencias
     */
    public function setParentid($parentid)
    {
        $this->parentid = $parentid;

        return $this;
    }

    /**
     * Get parentid
     *
     * @return int
     */
    public function getParentid()
    {
        return $this->parentid;
    }

    /**
     * Set fuente
     *
     * @param string $fuente
     *
     * @return Competencias
     */
    public function setFuente($fuente)
    {
        $this->fuente = $fuente;

        return $this;
    }

    /**
     * Get fuente
     *
     * @return string
     */
    public function getFuente()
    {
        return $this->fuente;
    }

    /**
     * Set area
     *
     * @param array $area
     *
     * @return Competencias
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return array
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set areaTitle
     *
     * @param string $areaTitle
     *
     * @return Competencias
     */
    public function setAreaTitle($areaTitle)
    {
        $this->areaTitle = $areaTitle;

        return $this;
    }

    /**
     * Get areaTitle
     *
     * @return string
     */
    public function getAreaTitle()
    {
        return $this->areaTitle;
    }

    /**
     * Set subArea
     *
     * @param string $subArea
     *
     * @return Competencias
     */
    public function setSubArea($subArea)
    {
        $this->subArea = $subArea;

        return $this;
    }

    /**
     * Get subArea
     *
     * @return string
     */
    public function getSubArea()
    {
        return $this->subArea;
    }

    /**
     * Set nivel
     *
     * @param string $nivel
     *
     * @return Competencias
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return string
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set etapa
     *
     * @param integer $etapa
     *
     * @return Competencias
     */
    public function setEtapa($etapa)
    {
        $this->etapa = $etapa;

        return $this;
    }

    /**
     * Get etapa
     *
     * @return int
     */
    public function getEtapa()
    {
        return $this->etapa;
    }

    /**
     * Set contenido
     *
     * @param string $contenido
     *
     * @return Competencias
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return string
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Competencias
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}

