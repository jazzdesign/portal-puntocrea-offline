<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CNB
 *
 * @ORM\Table(name="c_n_b")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CNBRepository")
 */
class CNB
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fuente", type="string", length=255)
     */
    private $fuente;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=1, nullable=true)
     */
    private $categoria;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=255, nullable=true)
     */
    private $area;

    /**
     * @var string
     *
     * @ORM\Column(name="sub_area", type="string", length=255, nullable=true)
     */
    private $subArea;

    /**
     * @var string
     *
     * @ORM\Column(name="nivel", type="string", length=255, nullable=true)
     */
    private $nivel;

    /**
     * @var string
     *
     * @ORM\Column(name="etapa", type="string", length=1, nullable=true)
     */
    private $etapa;

    /**
     * @var string
     *
     * @ORM\Column(name="competencia", type="text")
     */
    private $competencia;

    /**
     * @var string
     *
     * @ORM\Column(name="indicador", type="text")
     */
    private $indicador;

    /**
     * @var string
     *
     * @ORM\Column(name="saber_cognoscitivo", type="text")
     */
    private $saberCognoscitivo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fuente
     *
     * @param string $fuente
     *
     * @return CNB
     */
    public function setFuente($fuente)
    {
        $this->fuente = $fuente;

        return $this;
    }

    /**
     * Get fuente
     *
     * @return string
     */
    public function getFuente()
    {
        return $this->fuente;
    }

    /**
     * Set categoria
     *
     * @param string $categoria
     *
     * @return CNB
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set area
     *
     * @param string $area
     *
     * @return CNB
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set subArea
     *
     * @param string $subArea
     *
     * @return CNB
     */
    public function setSubArea($subArea)
    {
        $this->subArea = $subArea;

        return $this;
    }

    /**
     * Get subArea
     *
     * @return string
     */
    public function getSubArea()
    {
        return $this->subArea;
    }

    /**
     * Set nivel
     *
     * @param string $nivel
     *
     * @return CNB
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return string
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set etapa
     *
     * @param string $etapa
     *
     * @return CNB
     */
    public function setEtapa($etapa)
    {
        $this->etapa = $etapa;

        return $this;
    }

    /**
     * Get etapa
     *
     * @return string
     */
    public function getEtapa()
    {
        return $this->etapa;
    }

    /**
     * Set competencia
     *
     * @param string $competencia
     *
     * @return CNB
     */
    public function setCompetencia($competencia)
    {
        $this->competencia = $competencia;

        return $this;
    }

    /**
     * Get competencia
     *
     * @return string
     */
    public function getCompetencia()
    {
        return $this->competencia;
    }

    /**
     * Set indicador
     *
     * @param string $indicador
     *
     * @return CNB
     */
    public function setIndicador($indicador)
    {
        $this->indicador = $indicador;

        return $this;
    }

    /**
     * Get indicador
     *
     * @return string
     */
    public function getIndicador()
    {
        return $this->indicador;
    }

    /**
     * Set saberCognoscitivo
     *
     * @param string $saberCognoscitivo
     *
     * @return CNB
     */
    public function setSaberCognoscitivo($saberCognoscitivo)
    {
        $this->saberCognoscitivo = $saberCognoscitivo;

        return $this;
    }

    /**
     * Get saberCognoscitivo
     *
     * @return string
     */
    public function getSaberCognoscitivo()
    {
        return $this->saberCognoscitivo;
    }
}

