<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Calificacion
 *
 * @ORM\Table(name="calificacion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CalificacionRepository")
 */
class Calificacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="nota", type="integer")
     */
    private $nota;


    /**
     * @ORM\ManyToOne(targetEntity="Reto", inversedBy="calificaciones")
     * @ORM\JoinColumn(name="reto_id", referencedColumnName="id")
     */
    private $reto;

    /**
     * @ORM\ManyToOne(targetEntity="Participante", inversedBy="calificaciones")
     * @ORM\JoinColumn(name="participante_id", referencedColumnName="id")
     */
    private $participante;


    /**
     * @ORM\OneToMany(targetEntity="CalificacionRubrica", mappedBy="calificacion")
     */
    private $calificaciones_rubricas;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nota
     *
     * @param integer $nota
     *
     * @return Calificacion
     */
    public function setNota($nota)
    {
        $this->nota = $nota;

        return $this;
    }

    /**
     * Get nota
     *
     * @return int
     */
    public function getNota()
    {
        return $this->nota;
    }

    public function setReto($reto){
        $this->reto = $reto;
        
        return $this;
    }

    public function getReto(){
        return $this->reto;
    }

    public function setParticipante($participante){
        $this->participante = $participante;
        
        return $this;
    }

    public function getParticipante(){
        return $this->participante;
    }

    public function setCalificacionesRubricas($calificaciones_rubricas){
        $this->calificaciones_rubricas = $calificaciones_rubricas;
        
        return $this;
    }

    public function getCalificacionesRubricas(){
        return $this->calificaciones_rubricas;
    }
}

