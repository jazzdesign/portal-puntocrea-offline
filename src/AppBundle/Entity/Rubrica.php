<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rubrica
 *
 * @ORM\Table(name="rubrica")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RubricaRepository")
 */
class Rubrica
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_corta", type="text")
     */
    private $descripcionCorta;

    /**
     * @var string
     *
     * @ORM\Column(name="no_logro", type="text")
     */
    private $noLogro;

    /**
     * @var string
     *
     * @ORM\Column(name="parcial_logro", type="text")
     */
    private $parcialLogro;

    /**
     * @var string
     *
     * @ORM\Column(name="total_logro", type="text")
     */
    private $totalLogro;

    /**
     * @ORM\ManyToOne(targetEntity="Reto", inversedBy="rubricas")
     * @ORM\JoinColumn(name="reto_id", referencedColumnName="id")
     */
    private $reto;

     /**
     * @ORM\OneToMany(targetEntity="CalificacionRubrica", mappedBy="rubrica")
     */
    private $calificaciones_rubricas;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Rubrica
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    /**
     * Set descripcionCorta
     *
     * @param string $descripcionCorta
     *
     * @return Rubrica
     */
    public function setDescripcionCorta($descripcionCorta)
    {
        $this->descripcionCorta = $descripcionCorta;

        return $this;
    }

    /**
     * Get descripcionCorta
     *
     * @return string
     */
    public function getDescripcionCorta()
    {
        return $this->descripcionCorta;
    }

    /**
     * Set noLogro
     *
     * @param string $noLogro
     *
     * @return Rubrica
     */
    public function setNoLogro($noLogro)
    {
        $this->noLogro = $noLogro;

        return $this;
    }

    /**
     * Get noLogro
     *
     * @return string
     */
    public function getNoLogro()
    {
        return $this->noLogro;
    }

    /**
     * Set parcialLogro
     *
     * @param string $parcialLogro
     *
     * @return Rubrica
     */
    public function setParcialLogro($parcialLogro)
    {
        $this->parcialLogro = $parcialLogro;

        return $this;
    }

    /**
     * Get parcialLogro
     *
     * @return string
     */
    public function getParcialLogro()
    {
        return $this->parcialLogro;
    }

    /**
     * Set totalLogro
     *
     * @param string $totalLogro
     *
     * @return Rubrica
     */
    public function setTotalLogro($totalLogro)
    {
        $this->totalLogro = $totalLogro;

        return $this;
    }

    /**
     * Get totalLogro
     *
     * @return string
     */
    public function getTotalLogro()
    {
        return $this->totalLogro;
    }



    public function setReto($reto){
        $this->reto = $reto;

        return $this;
    }

    public function getReto(){
        return $this->reto;
    }
}

