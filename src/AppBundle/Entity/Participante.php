<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Participante
 *
 * @ORM\Table(name="participante")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ParticipanteRepository")
 */
class Participante
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="cui", type="bigint")
     */
    private $cui;

    /**
     * @var string
     *
     * @ORM\Column(name="nombres", type="string", length=255)
     */
    private $nombres;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=255)
     */
    private $apellidos;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_nacimiento", type="datetimetz")
     */
    private $fechaNacimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="genero", type="string", length=1)
     */
    private $genero;

    /**
     * @var int
     *
     * @ORM\Column(name="telefono_casa", type="integer")
     */
    private $telefonoCasa;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono_celular", type="string", length=255)
     */
    private $telefonoCelular;

    /**
     * @var string
     *
     * @ORM\Column(name="comunidad", type="string", length=255)
     */
    private $comunidad;

    /**
     * @var string
     *
     * @ORM\Column(name="municipio", type="string", length=255)
     */
    private $municipio;

    /**
     * @var string
     *
     * @ORM\Column(name="departamento", type="string", length=255)
     */
    private $departamento;

    /**
     * @var string
     * 
     * @ORM\Column(name="nombre_encargado", type="string", length=255, nullable=true)
     */
    private $nombreEncargado;

    /**
     * @var string
     * 
     * @ORM\Column(name="parentezco_encargado", type="string", length=255, nullable=true)
     */
    private $parentezcoEncargado;

    /**
     * @var int
     *
     * @ORM\Column(name="telefono_encargado", type="integer", nullable=true)
     */
    private $telefonoEncargado;



     /**
     * One Participante has One User.
     * @ORM\OneToOne(targetEntity="User", inversedBy="participante")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    /**
     * @ORM\OneToMany(targetEntity="Calificacion", mappedBy="participante")
     */
    private $calificaciones;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cui
     *
     * @param integer $cui
     *
     * @return Participante
     */
    public function setCui($cui)
    {
        $this->cui = $cui;

        return $this;
    }

    /**
     * Get cui
     *
     * @return int
     */
    public function getCui()
    {
        return $this->cui;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     *
     * @return Participante
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * Get nombres
     *
     * @return string
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     *
     * @return Participante
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     *
     * @return Participante
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set genero
     *
     * @param string $genero
     *
     * @return Participante
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return string
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set telefonoCasa
     *
     * @param integer $telefonoCasa
     *
     * @return Participante
     */
    public function setTelefonoCasa($telefonoCasa)
    {
        $this->telefonoCasa = $telefonoCasa;

        return $this;
    }

    /**
     * Get telefonoCasa
     *
     * @return int
     */
    public function getTelefonoCasa()
    {
        return $this->telefonoCasa;
    }

    /**
     * Set telefonoCelular
     *
     * @param string $telefonoCelular
     *
     * @return Participante
     */
    public function setTelefonoCelular($telefonoCelular)
    {
        $this->telefonoCelular = $telefonoCelular;

        return $this;
    }

    /**
     * Get telefonoCelular
     *
     * @return string
     */
    public function getTelefonoCelular()
    {
        return $this->telefonoCelular;
    }

    /**
     * Set comunidad
     *
     * @param string $comunidad
     *
     * @return Participante
     */
    public function setComunidad($comunidad)
    {
        $this->comunidad = $comunidad;

        return $this;
    }

    /**
     * Get comunidad
     *
     * @return string
     */
    public function getComunidad()
    {
        return $this->comunidad;
    }

    /**
     * Set municipio
     *
     * @param string $municipio
     *
     * @return Participante
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return string
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set departamento
     *
     * @param string $departamento
     *
     * @return Participante
     */
    public function setDepartamento($departamento)
    {
        $this->departamento = $departamento;

        return $this;
    }

    /**
     * Get departamento
     *
     * @return string
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * Set nombreEncargado
     *
     * @param string $nombreEncargado
     *
     * @return Participante
     */
    public function setNombreEncargado($nombreEncargado)
    {
        $this->nombreEncargado = $nombreEncargado;

        return $this;
    }

    /*
     * Get nombreEncargado
     *
     * @return string
     */
    public function getNombreEncargado()
    {
        return $this->nombreEncargado;
    }

    /**
     * Set parentezcoEncargado
     *
     * @param string $parentezcoEncargado
     *
     * @return Participante
     */
    public function setParentezcoEncargado($parentezcoEncargado)
    {
        $this->parentezcoEncargado = $parentezcoEncargado;

        return $this;
    }

    /*
     * Get parentezcoEncargado
     *
     * @return string
     */
    public function getParentezcoEncargado()
    {
        return $this->parentezcoEncargado;
    }

    /**
     * Set telefonoEncargado
     *
     * @param integer $telefonoEncargado
     *
     * @return Participante
     */
    public function setTelefonoEncargado($telefonoEncargado)
    {
        $this->telefonoEncargado = $telefonoEncargado;

        return $this;
    }

    /**
     * Get telefonoEncargado
     *
     * @return int
     */
    public function getTelefonoEncargado()
    {
        return $this->telefonoEncargado;
    }

    public function getUser(){      
        return $this->user;
    }

    public function setUser($user){
        $this->user = $user;
    }
}

