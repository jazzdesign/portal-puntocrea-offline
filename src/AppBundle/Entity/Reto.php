<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reto
 *
 * @ORM\Table(name="reto")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RetoRepository")
 */
class Reto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text")
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="category", type="text", nullable=true)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="text", nullable=true)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="situacion", type="text")
     */
    private $situacion;

    /**
     * @var string
     *
     * @ORM\Column(name="pregunta", type="text")
     */
    private $pregunta;

    /**
     * @var string
     *
     * @ORM\Column(name="investigo", type="text")
     */
    private $investigo;

    /**
     * @var string
     *
     * @ORM\Column(name="materiales", type="text")
     */
    private $materiales;

    /**
     * @var string
     *
     * @ORM\Column(name="ensayo", type="text")
     */
    private $ensayo;

    /**
     * @var string
     *
     * @ORM\Column(name="conocer", type="text")
     */
    private $conocer;

    /**
     * @var string
     *
     * @ORM\Column(name="aplicar", type="text")
     */
    private $aplicar;

    /**
     * @ORM\OneToMany(targetEntity="Rubrica", mappedBy="reto")
     */
    private $rubricas;

    /**
     * @ORM\OneToMany(targetEntity="Calificacion", mappedBy="reto")
     */
    private $calificaciones;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Reto
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return Reto
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set level
     *
     * @param string $level
     *
     * @return Reto
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set situacion
     *
     * @param string $situacion
     *
     * @return Reto
     */
    public function setSituacion($situacion)
    {
        $this->situacion = $situacion;

        return $this;
    }

    /**
     * Get situacion
     *
     * @return string
     */
    public function getSituacion()
    {
        return $this->situacion;
    }

    /**
     * Set pregunta
     *
     * @param string $pregunta
     *
     * @return Reto
     */
    public function setPregunta($pregunta)
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta
     *
     * @return string
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Set investigo
     *
     * @param string $investigo
     *
     * @return Reto
     */
    public function setInvestigo($investigo)
    {
        $this->investigo = $investigo;

        return $this;
    }

    /**
     * Get investigo
     *
     * @return string
     */
    public function getInvestigo()
    {
        return $this->investigo;
    }

    /**
     * Set materiales
     *
     * @param string $materiales
     *
     * @return Reto
     */
    public function setMateriales($materiales)
    {
        $this->materiales = $materiales;

        return $this;
    }

    /**
     * Get materiales
     *
     * @return string
     */
    public function getMateriales()
    {
        return $this->materiales;
    }

    /**
     * Set ensayo
     *
     * @param string $ensayo
     *
     * @return Reto
     */
    public function setEnsayo($ensayo)
    {
        $this->ensayo = $ensayo;

        return $this;
    }

    /**
     * Get ensayo
     *
     * @return string
     */
    public function getEnsayo()
    {
        return $this->ensayo;
    }


    /**
     * Set conocer
     *
     * @param string $conocer
     *
     * @return Reto
     */
    public function setConocer($conocer)
    {
        $this->conocer = $conocer;

        return $this;
    }

    /**
     * Get conocer
     *
     * @return string
     */
    public function getConocer()
    {
        return $this->conocer;
    }

    /**
     * Set aplicar
     *
     * @param string $aplicar
     *
     * @return Reto
     */
    public function setAplicar($aplicar)
    {
        $this->aplicar = $aplicar;

        return $this;
    }

    /**
     * Get aplicar
     *
     * @return string
     */
    public function getAplicar()
    {
        return $this->aplicar;
    }


    public function setRubricas($rubricas){
        $this->rubricas = $rubricas;
        
        return $this;
    }

    public function getRubricas(){
        return $this->rubricas;
    }

    public function setCalificaciones($calificaciones){
        $this->calificaciones = $calificaciones;
        
        return $this;
    }

    public function getCalificaciones(){
        return $this->calificaciones;
    }
}

