<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * competodas
 *
 * @ORM\Table(name="competodas")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\competodasRepository")
 */
class competodas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="competencia", type="text")
     */
    private $competencia;

    /**
     * @var string
     *
     * @ORM\Column(name="indicador", type="text")
     */
    private $indicador;

    /**
     * @var string
     *
     * @ORM\Column(name="saberCognoscitivo", type="text")
     */
    private $saberCognoscitivo;

    /**
     * @var string
     *
     * @ORM\Column(name="ciencia", type="string", length=1, nullable=true)
     */
    private $ciencia;

    /**
     * @var string
     *
     * @ORM\Column(name="tecnologia", type="string", length=1, nullable=true)
     */
    private $tecnologia;

    /**
     * @var string
     *
     * @ORM\Column(name="ingenieria", type="string", length=1, nullable=true)
     */
    private $ingenieria;

    /**
     * @var string
     *
     * @ORM\Column(name="artes", type="string", length=1, nullable=true)
     */
    private $artes;

    /**
     * @var string
     *
     * @ORM\Column(name="matematica", type="string", length=1, nullable=true)
     */
    private $matematica;

    /**
     * @var string
     *
     * @ORM\Column(name="socio_emocionales", type="string", length=1, nullable=true)
     */
    private $socioEmocionales;

    /**
     * @var string
     *
     * @ORM\Column(name="saber_pensar", type="string", length=1, nullable=true)
     */
    private $saberPensar;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set competencia
     *
     * @param string $competencia
     *
     * @return competodas
     */
    public function setCompetencia($competencia)
    {
        $this->competencia = $competencia;

        return $this;
    }

    /**
     * Get competencia
     *
     * @return string
     */
    public function getCompetencia()
    {
        return $this->competencia;
    }

    /**
     * Set indicador
     *
     * @param string $indicador
     *
     * @return competodas
     */
    public function setIndicador($indicador)
    {
        $this->indicador = $indicador;

        return $this;
    }

    /**
     * Get indicador
     *
     * @return string
     */
    public function getIndicador()
    {
        return $this->indicador;
    }

    /**
     * Set saberCognoscitivo
     *
     * @param string $saberCognoscitivo
     *
     * @return competodas
     */
    public function setSaberCognoscitivo($saberCognoscitivo)
    {
        $this->saberCognoscitivo = $saberCognoscitivo;

        return $this;
    }

    /**
     * Get saberCognoscitivo
     *
     * @return string
     */
    public function getSaberCognoscitivo()
    {
        return $this->saberCognoscitivo;
    }

    /**
     * Set ciencia
     *
     * @param string $ciencia
     *
     * @return competodas
     */
    public function setCiencia($ciencia)
    {
        $this->ciencia = $ciencia;

        return $this;
    }

    /**
     * Get ciencia
     *
     * @return string
     */
    public function getCiencia()
    {
        return $this->ciencia;
    }

    /**
     * Set tecnologia
     *
     * @param string $tecnologia
     *
     * @return competodas
     */
    public function setTecnologia($tecnologia)
    {
        $this->tecnologia = $tecnologia;

        return $this;
    }

    /**
     * Get tecnologia
     *
     * @return string
     */
    public function getTecnologia()
    {
        return $this->tecnologia;
    }

    /**
     * Set ingenieria
     *
     * @param string $ingenieria
     *
     * @return competodas
     */
    public function setIngenieria($ingenieria)
    {
        $this->ingenieria = $ingenieria;

        return $this;
    }

    /**
     * Get ingenieria
     *
     * @return string
     */
    public function getIngenieria()
    {
        return $this->ingenieria;
    }

    /**
     * Set artes
     *
     * @param string $artes
     *
     * @return competodas
     */
    public function setArtes($artes)
    {
        $this->artes = $artes;

        return $this;
    }

    /**
     * Get artes
     *
     * @return string
     */
    public function getArtes()
    {
        return $this->artes;
    }

    /**
     * Set matematica
     *
     * @param string $matematica
     *
     * @return competodas
     */
    public function setMatematica($matematica)
    {
        $this->matematica = $matematica;

        return $this;
    }

    /**
     * Get matematica
     *
     * @return string
     */
    public function getMatematica()
    {
        return $this->matematica;
    }

    /**
     * Set socioEmocionales
     *
     * @param string $socioEmocionales
     *
     * @return competodas
     */
    public function setSocioEmocionales($socioEmocionales)
    {
        $this->socioEmocionales = $socioEmocionales;

        return $this;
    }

    /**
     * Get socioEmocionales
     *
     * @return string
     */
    public function getSocioEmocionales()
    {
        return $this->socioEmocionales;
    }

    /**
     * Set saberPensar
     *
     * @param string $saberPensar
     *
     * @return competodas
     */
    public function setSaberPensar($saberPensar)
    {
        $this->saberPensar = $saberPensar;

        return $this;
    }

    /**
     * Get saberPensar
     *
     * @return string
     */
    public function getSaberPensar()
    {
        return $this->saberPensar;
    }
}

