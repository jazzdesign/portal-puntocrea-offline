<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CBV
 *
 * @ORM\Table(name="c_b_v")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CBVRepository")
 */
class CBV
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fuente", type="string", length=255)
     */
    private $fuente;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=1, nullable=true)
     */
    private $categoria;

    /**
     * @var string
     *
     * @ORM\Column(name="competencia", type="text")
     */
    private $competencia;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="dimension_clave", type="text")
     */
    private $dimensionClave;

    /**
     * @var string
     *
     * @ORM\Column(name="indicador_logro", type="text")
     */
    private $indicadorLogro;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fuente
     *
     * @param string $fuente
     *
     * @return CBV
     */
    public function setFuente($fuente)
    {
        $this->fuente = $fuente;

        return $this;
    }

    /**
     * Get fuente
     *
     * @return string
     */
    public function getFuente()
    {
        return $this->fuente;
    }

    /**
     * Set categoria
     *
     * @param string $categoria
     *
     * @return CBV
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set competencia
     *
     * @param string $competencia
     *
     * @return CBV
     */
    public function setCompetencia($competencia)
    {
        $this->competencia = $competencia;

        return $this;
    }

    /**
     * Get competencia
     *
     * @return string
     */
    public function getCompetencia()
    {
        return $this->competencia;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return CBV
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set dimensionClave
     *
     * @param string $dimensionClave
     *
     * @return CBV
     */
    public function setDimensionClave($dimensionClave)
    {
        $this->dimensionClave = $dimensionClave;

        return $this;
    }

    /**
     * Get dimensionClave
     *
     * @return string
     */
    public function getDimensionClave()
    {
        return $this->dimensionClave;
    }

    /**
     * Set indicadorLogro
     *
     * @param string $indicadorLogro
     *
     * @return CBV
     */
    public function setIndicadorLogro($indicadorLogro)
    {
        $this->indicadorLogro = $indicadorLogro;

        return $this;
    }

    /**
     * Get indicadorLogro
     *
     * @return string
     */
    public function getIndicadorLogro()
    {
        return $this->indicadorLogro;
    }
}

