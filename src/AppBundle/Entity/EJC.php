<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EJC
 *
 * @ORM\Table(name="e_j_c")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EJCRepository")
 */
class EJC
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fuente", type="string", length=255)
     */
    private $fuente;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=1, nullable=true)
     */
    private $categoria;

    /**
     * @var string
     *
     * @ORM\Column(name="competencia", type="text")
     */
    private $competencia;

    /**
     * @var string
     *
     * @ORM\Column(name="indicador_logro", type="text")
     */
    private $indicadorLogro;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text")
     */
    private $contenido;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fuente
     *
     * @param string $fuente
     *
     * @return EJC
     */
    public function setFuente($fuente)
    {
        $this->fuente = $fuente;

        return $this;
    }

    /**
     * Get fuente
     *
     * @return string
     */
    public function getFuente()
    {
        return $this->fuente;
    }

    /**
     * Set categoria
     *
     * @param string $categoria
     *
     * @return EJC
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set competencia
     *
     * @param string $competencia
     *
     * @return EJC
     */
    public function setCompetencia($competencia)
    {
        $this->competencia = $competencia;

        return $this;
    }

    /**
     * Get competencia
     *
     * @return string
     */
    public function getCompetencia()
    {
        return $this->competencia;
    }

    /**
     * Set indicadorLogro
     *
     * @param string $indicadorLogro
     *
     * @return EJC
     */
    public function setIndicadorLogro($indicadorLogro)
    {
        $this->indicadorLogro = $indicadorLogro;

        return $this;
    }

    /**
     * Get indicadorLogro
     *
     * @return string
     */
    public function getIndicadorLogro()
    {
        return $this->indicadorLogro;
    }

    /**
     * Set contenido
     *
     * @param string $contenido
     *
     * @return EJC
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return string
     */
    public function getContenido()
    {
        return $this->contenido;
    }
}

