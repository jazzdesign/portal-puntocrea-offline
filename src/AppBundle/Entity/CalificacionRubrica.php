<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CalificacionRubrica
 *
 * @ORM\Table(name="calificacion_rubrica")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CalificacionRubricaRepository")
 */
class CalificacionRubrica
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


     /**
     * @ORM\ManyToOne(targetEntity="Rubrica", inversedBy="calificaciones_rubricas")
     * @ORM\JoinColumn(name="rubrica_id", referencedColumnName="id")
     */
    private $rubrica;

    /**
     * @ORM\ManyToOne(targetEntity="Calificacion", inversedBy="calificaciones_rubricas")
     * @ORM\JoinColumn(name="calificacion_id", referencedColumnName="id")
     */
    private $calificacion;


    /**
     * @var int
     *
     * @ORM\Column(name="nota", type="integer")
     */
    private $nota;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nota
     *
     * @param integer $nota
     *
     * @return CalificacionRubrica
     */
    public function setNota($nota)
    {
        $this->nota = $nota;

        return $this;
    }

    /**
     * Get nota
     *
     * @return int
     */
    public function getNota()
    {
        return $this->nota;
    }

    public function setRubrica($rubrica) {
        $this->rubrica = $rubrica;
        
        return $this;
    }

    public function setCalificacion($calificacion){
        $this->calificacion = $calificacion;

        return $this;
    }
}

