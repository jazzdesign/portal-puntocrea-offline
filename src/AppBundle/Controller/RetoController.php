<?php
  namespace AppBundle\Controller;

 
  use AppBundle\Entity\Reto;
  use AppBundle\Entity\Rubrica;
  use AppBundle\Entity\Participante;
  use AppBundle\Entity\Calificacion;
  use AppBundle\Entity\CalificacionRubrica;
  use AppBundle\Form\RubricaType;
  use Symfony\Component\HttpFoundation\Response;
  use Symfony\Component\HttpFoundation\Request;

  use Symfony\Component\Routing\Annotation\Route;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;

  use Symfony\Component\Form\Extension\Core\Type\TextType;
  use Symfony\Component\Form\Extension\Core\Type\TextareaType;
  use Symfony\Component\Form\Extension\Core\Type\IntegerType;
  use Symfony\Component\Form\Extension\Core\Type\SubmitType;
  use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


  class RetoController extends Controller{
    /**
     * @Route("/retos", name="reto_list")
     * @Method({"GET"})
     */

    public function index(){
      $retos = $this->getDoctrine()->getRepository(Reto::class)->findAll();
      
      // $builder = $this->createFormBuilder(array());
      
      // $builder = $builder->add('category', ChoiceType::class, array(
      //                         'label' => 'Category',
      //                         'attr' => array('class' => 'form-control'),
      //                         'choices'  => array(
      //                                       'No Logro' => '0',
      //                                       'Logro Parcial' => '1',
      //                                       'Logro Total' => '2',
      //                                       ),
      //                       ));

      // $form = $builder->getForm();

      return $this->render('retos/list.html.twig', array('retos' => $retos));
    }


    /**
     * @Route("/retos/crear", name="new_reto")
     * @Method({"GET", "POST"})
     */

    public function nuevo(Request $request){
      $reto = new Reto();

        $form = $this->createFormBuilder($reto)
          ->add('category', ChoiceType::class, array(
                'label' => 'Categoría',
                'required'   => false,
                'attr' => array('class' => 'form-control'),
                'choices'  => array(
                    'Inspiracional' => 'Inspiracional',
                    'Ciencia' => 'Ciencia',
                    'Tecnología' => 'Tecnología',
                    'Ingeniería' => 'Ingeniería',
                    'Arte' => 'Arte',
                    'Matemática' => 'Matemática',
                    ),
                ))
          ->add('level', ChoiceType::class, array(
                'label' => 'Nivel',
                'required'   => false,
                'attr' => array('class' => 'form-control'),
                'choices'  => array(
                    'Inicial' => 'Inicial',
                    'Intermedio' => 'Intermedio',
                    'Final' => 'Final',
                    ),
                ))
          ->add('title', TextType::class, array(
            'label' => ' ',
            'attr' => array('class' => 'form-control')))

          ->add('situacion', TextareaType::class, array(
            'label' => ' ',
            'required' => false,
            'attr' => array('id' => 'fieldSituacion', 'class' => 'form-control', 'style' => 'display:none!important')
          ))
          ->add('pregunta', TextareaType::class, array(
            'required' => false,
            'attr' => array('id' => 'fieldPregunta', 'class' => 'form-control', 'style' => 'display:none!important')
          ))
          ->add('investigo', TextareaType::class, array(
            'required' => false,
            'attr' => array('id' => 'fieldInvestigo', 'class' => 'form-control', 'style' => 'display:none!important')
          ))
          ->add('materiales', TextareaType::class, array(
            'required' => false,
            'attr' => array('id' => 'fieldMateriales', 'class' => 'form-control', 'style' => 'display:none!important')
          ))
          ->add('ensayo', TextareaType::class, array(
            'required' => false,
            'attr' => array('id' => 'fieldEnsayo', 'class' => 'form-control', 'style' => 'display:none!important')
          ))
          ->add('conocer', TextareaType::class, array(
            'required' => false,
            'attr' => array('id' => 'fieldConocer', 'class' => 'form-control', 'style' => 'display:none!important')
          ))
          ->add('aplicar', TextareaType::class, array(
            'required' => false,
            'attr' => array('id' => 'fieldAplicar', 'class' => 'form-control', 'style' => 'display:none!important')
          ))

          ->add('save', SubmitType::class, array(
            'label' => 'Crear',
            'attr' => array('class' => 'btn btn-primary btn-lg btn-block mt-3 mb-5')
          ))
          ->getForm();

          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid()){
            $reto = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($reto);
            $entityManager->flush();

            return $this->redirectToRoute('reto_list');
          }

          return $this->render('retos/new.html.twig', array(
            'form' => $form->createView()
          ));
    }

    // /**
    //  * @Route("/retos/reto/{id}", name="reto_show")
    //  */

    //  public function show($id){
    //    $reto = $this->getDoctrine()->getRepository(Reto::class)->find($id);

    //    return $this->render('retos/show.html.twig', array('reto' => $reto));
    //  }


    /**
       * @Route("/retos/borrar/{id}", name="delete_reto")
       */
       public function delete(Request $request, $id){
          $reto = $this->getDoctrine()->getRepository(Reto::class)->find($id);

          $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($reto);
            $entityManager->flush();

            // $response = new Response();
            // $response->send();
            return $this->redirectToRoute('reto_list');
       }

       //VERSION ORIGINAL
      //         * Method({"DELETE"})
      //  public function delete(Request $request, $id){
      //     $reto = $this->getDoctrine()->getRepository(Reto::class)->find($id);

      //     $entityManager = $this->getDoctrine()->getManager();
      //       $entityManager->remove($reto);
      //       $entityManager->flush();

      //       // $response = new Response();
      //       // $response->send();
      //       return $this->redirectToRoute('reto_list');
      //  }


      /**
        * @Route("/retos/reto/{id}", name="edit_reto")
        * Method({"GET", "POST"})
        */
        
        public function edit(Request $request, $id){
          $reto = new Reto();
          $reto = $this->getDoctrine()->getRepository(Reto::class)->find($id);

          $form = $this->createFormBuilder($reto)
            ->add('category', ChoiceType::class, array(
                  'label' => 'Categoría',
                  'required'   => false,
                  'attr' => array('class' => 'form-control'),
                  'choices'  => array(
                      'Inspiracional' => 'Inspiracional',
                      'Ciencia' => 'Ciencia',
                      'Tecnología' => 'Tecnología',
                      'Ingeniería' => 'Ingeniería',
                      'Arte' => 'Arte',
                      'Matemática' => 'Matemática',
                      ),
                  ))
            ->add('level', ChoiceType::class, array(
                  'label' => 'Nivel',
                  'required'   => false,
                  'attr' => array('class' => 'form-control'),
                  'choices'  => array(
                      'Inicial' => 'Inicial',
                      'Intermedio' => 'Intermedio',
                      'Final' => 'Final',
                      ),
                  ))
            ->add('title', TextType::class, array(
            'label' => ' ',
            'attr' => array('class' => 'form-control')))

            ->add('situacion', TextareaType::class, array(
              'label' => ' ',
              'required' => true,
              'attr' => array('id' => 'summernoteSituacion', 'class' => 'form-control', 'style' => 'display:none')
            ))
            ->add('pregunta', TextareaType::class, array(
              'required' => true,
              'attr' => array('id' => 'summernotePregunta', 'class' => 'form-control', 'style' => 'display:none')
            ))
            ->add('investigo', TextareaType::class, array(
              'required' => true,
              'attr' => array('id' => 'summernoteInvestigo', 'class' => 'form-control', 'style' => 'display:none')
            ))
            ->add('materiales', TextareaType::class, array(
              'required' => true,
              'attr' => array('id' => 'summernoteMateriales', 'class' => 'form-control', 'style' => 'display:none')
            ))
            ->add('ensayo', TextareaType::class, array(
              'required' => true,
              'attr' => array('id' => 'summernoteEnsayo', 'class' => 'form-control', 'style' => 'display:none')
            ))
            ->add('conocer', TextareaType::class, array(
              'required' => true,
              'attr' => array('id' => 'summernoteConocer', 'class' => 'form-control', 'style' => 'display:none')
            ))
            ->add('aplicar', TextareaType::class, array(
              'required' => true,
              'attr' => array('id' => 'summernoteAplicar', 'class' => 'form-control ', 'style' => 'display:none')
            ))
            ->add('save', SubmitType::class, array(
              'label' => 'Guardar',
              'attr' => array('class' => 'btn btn-primary btn-lg btn-block mt-3 mb-5')
            ))
            ->getForm();

          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('edit_reto', array( 'id' => $id));
          }

          return $this->render('retos/show.html.twig', array(
            'form' => $form->createView(), 'reto' => $reto
          ));

        }


     /**
     * @Route("/retos/reto/{id}/aspectos", name="aspectos_reto")
     * @Method({"GET", "POST"})
     */
      public function aspectos($id, Request $request){
          $message = null;
          $reto = $this->getDoctrine()->getRepository(Reto::class)->find($id);

          $rubricas = $this->getDoctrine()->getRepository(Rubrica::class)->findBy(array('reto' => $reto));

          $rubrica = new Rubrica();
          $rubrica->setReto($reto);
          $form = $this->createForm(RubricaType::class, $rubrica);
          $form->handleRequest($request);

          if ($form->isSubmitted() && $form->isValid()) {

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($rubrica);
            $entityManager->flush();

            return $this->redirectToRoute('aspectos_reto', array('id' => $id));
            $message = 'Aspecto Ingresado!';
            // return $this->redirectToRoute('user_registration2', array('user_id' => $user->id));


        }
   
        return $this->render(
            'retos/aspectos.html.twig',
            array('form' => $form->createView(), 'message' => $message, 'rubricas' => $rubricas, 'reto' => $reto)
        );
      }

      /**
     * @Route("/retos/reto/{id}/aspectos/editar/{id_aspecto}", name="editar_aspectos")
     * @Method({"GET", "POST"})
     */
      public function aspectosEditar($id, $id_aspecto, Request $request){
          $message = null;
          $rubrica = new Rubrica();
          $reto = $this->getDoctrine()->getRepository(Reto::class)->find($id);

          $rubrica = $this->getDoctrine()->getRepository(Rubrica::class)->find($id_aspecto);

          // $rubrica->setReto($reto);
          $form = $this->createForm(RubricaType::class, $rubrica);
          $form->handleRequest($request);

          if ($form->isSubmitted() && $form->isValid()) {

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            // $entityManager->persist($rubrica);
            $entityManager->flush();

            return $this->redirectToRoute('aspectos_reto', array('id' => $id));
            $message = 'Aspecto Ingresado!';
            // return $this->redirectToRoute('user_registration2', array('user_id' => $user->id));


        }
   
        return $this->render(
            'retos/edit-aspectos.html.twig',
            array('form' => $form->createView(), 'message' => $message, 'rubrica' => $rubrica, 'reto' => $reto)
        );
      }

      /**
       * @Route("/reto/{id_reto}/aspecto/{id_aspecto}/borrar", name="delete_aspecto")
       */
       public function deleteAspecto(Request $request, $id_reto, $id_aspecto){
          $rubrica = $this->getDoctrine()->getRepository(Rubrica::class)->find($id_aspecto);

          $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($rubrica);
            $entityManager->flush();

            // $response = new Response();
            // $response->send();
            return $this->redirectToRoute('aspectos_reto', array('id' => $id_reto));
       }

     /**
     * @Route("/retos/reto/{reto_id}/evaluar", name="evaluar_reto")
     * @Method({"GET", "POST"})
     */

     public function evaluarReto($reto_id){ 

      $participantes = $this->getDoctrine()->getRepository(Participante::class)->findAll();
      
      return $this->render('retos/list-participantes.html.twig', array('reto_id' => $reto_id, 'participantes' => $participantes));

    }

    /**
     * @Route("/retos/reto/{reto_id}/evaluar-participante/{participante_id}", name="evaluar_participante")
     * @Method({"GET", "POST"})
     */

     public function evaluarParticipante($reto_id, $participante_id, Request $request){ 
      $reto = $this->getDoctrine()->getRepository(Reto::class)->find($reto_id);
      $participante = $this->getDoctrine()->getRepository(Participante::class)->find($participante_id);
      $rubricas = $this->getDoctrine()->getRepository(Rubrica::class)->findBy(array('reto' => $reto));

      $builder = $this->createFormBuilder(array());

      foreach ($rubricas as $rubrica) {
          $builder = $builder->add('rubrica-' . $rubrica->getId(), ChoiceType::class,  
                                    array(
                                      'label' => $rubrica->getAspecto(),
                                      'attr' => array('class' => 'form-control'),
                                      'choices'  => array(
                                                    'No Logro' => '0',
                                                    'Logro Parcial' => '1',
                                                    'Logro Total' => '2',
                                                    ),
                                    ) );
      }
        
      $form = $builder->getForm();

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {


          $data = $form->getData();
          
          $calificacion = new Calificacion();

          $calificacion->setReto($reto);
          $calificacion->setParticipante($participante);
          $calificacion->setNota(0);
          $em->persist($calificacion);
      
          $cr = array();
          foreach ($rubricas as $rubrica) {
            $nota = $data['rubrica-' . $rubrica->getId()];
            $calificacionRubrica = new CalificacionRubrica();
            $calificacionRubrica->setCalificacion($calificacion);
            $calificacionRubrica->setRubrica($rubrica);
            $calificacionRubrica->setNota($nota);
            $em->persist($calificacionRubrica);
            $cr[] = $calificacionRubrica;
          }
          $calificacion->setCalificacionesRubricas($cr);
          $em->persist($calificacion);
          $em->flush();
          $em->getConnection()->commit();
        } catch(Exception $e) {
          $em->getConnection()->rollBack();
        }
        return $this->redirectToRoute('evaluar_participante', array('reto_id' => $reto_id, 'participante_id' => $participante_id));
      }
      
      return $this->render('retos/participantes.html.twig', array('reto_id' => $reto_id, 'form' => $form->createView()));

    }
    
  }