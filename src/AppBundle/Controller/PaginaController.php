<?php

namespace AppBundle\Controller;

 use AppBundle\Entity\Pagina;
  use Symfony\Component\HttpFoundation\Response;
  use Symfony\Component\HttpFoundation\Request;

  use Symfony\Component\Routing\Annotation\Route;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;

  use Symfony\Component\Form\Extension\Core\Type\TextType;
  use Symfony\Component\Form\Extension\Core\Type\TextareaType;
  use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class PaginaController extends Controller
{

  /**
   * @Route("/paginas", name="paginas_list")
   * @Method({"GET"})
   */

  public function listaPagina(){
    $paginas = $this->getDoctrine()->getRepository(Pagina::class)->findAll();
    return $this->render('paginas/list-pages.html.twig', array('paginas' => $paginas));
  }

    /**
     * @Route("/paginas/nueva-pagina", name="new_page")
     * @Method({"GET", "POST"})
     */

   public function nuevaPagina(Request $request){
      $pagina = new Pagina();

        $form = $this->createFormBuilder($pagina)
          ->add('titulo', TextType::class, array(
            'label' => ' ',
            'attr' => array('class' => 'form-control')))

          ->add('body', TextareaType::class, array(
            'label' => ' ',
            'required' => false,
            'attr' => array('id' => 'fieldSituacion', 'class' => 'form-control', 'style' => 'display:none!important')
          ))
        
          ->add('save', SubmitType::class, array(
            'label' => 'Crear',
            'attr' => array('class' => 'btn btn-primary btn-lg btn-block mt-3 mb-5')
          ))
          ->getForm();

          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid()){
            $pagina = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pagina);
            $entityManager->flush();

            return $this->redirectToRoute('paginas_list');
          }

          return $this->render('paginas/new-page.html.twig', array(
            'form' => $form->createView()
          ));
    }
     /**
    * @Route("/paginas/{pagina_id}", name="edit_pagina")
    * Method({"GET", "POST"})
    */
      public function editPagina($pagina_id, Request $request){
        $pagina = new Pagina();
        $pagina = $this->getDoctrine()->getRepository(Pagina::class)->find($pagina_id);

        $form = $this->createFormBuilder($pagina)
          ->add('titulo', TextType::class, array(
          'label' => ' ',
          'attr' => array('class' => 'form-control')))

          ->add('body', TextareaType::class, array(
            'label' => ' ',
            'required' => true,
            'attr' => array('id' => 'summernoteSituacion', 'class' => 'form-control', 'style' => 'display:none')
          ))
          
          ->add('save', SubmitType::class, array(
            'label' => 'Guardar Edición',
            'attr' => array('class' => 'btn btn-primary btn-lg btn-block mt-3 mb-5')
          ))
          ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->flush();

          return $this->redirectToRoute('paginas_list');
        }

        return $this->render('paginas/edit-pagina.html.twig', array(
          'form' => $form->createView(), 'pagina' => $pagina
        ));

      }

    // /**
    //  * @Route("/paginas/{pagina_id}", name="pagina_show")
    //  */

    //  public function show($pagina_id){
    //    $pagina = $this->getDoctrine()->getRepository(Pagina::class)->find($pagina_id);

    //    return $this->render('paginas/mostrar-pagina.html.twig', array('pagina' => $pagina));
    //  }
}
