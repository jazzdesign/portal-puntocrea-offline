<?php

namespace AppBundle\Controller;

 use AppBundle\Entity\Participante;
  use Symfony\Component\HttpFoundation\Response;
  use Symfony\Component\HttpFoundation\Request;

  use Symfony\Component\Routing\Annotation\Route;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;

  use Symfony\Component\Form\Extension\Core\Type\TextType;
  use Symfony\Component\Form\Extension\Core\Type\TextareaType;
  use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class AlumnosController extends Controller
{
    /**
     * @Route("/alumnos", name="alumnos_list")
     * @Method({"GET"})
     */

    public function alumnos(){
      $alumnos = $this->getDoctrine()->getRepository(Participante::class)->findAll();
      return $this->render('alumnos/list.html.twig', array('alumnos' => $alumnos));
    }

}
