<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pagina;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage", defaults={"pagina_id" = 1})
     * 
     */
    public function indexAction($pagina_id, Request $request)
    {

        $pagina = $this->getDoctrine()->getRepository(Pagina::class)->find($pagina_id);
        if(!empty($pagina)){
            return $this->render('paginas/mostrar-pagina.html.twig', array('pagina' => $pagina));
        }else{
            return $this->render('/index.html.twig');
        }
    }

    /**
     * @Route("/competencias", name="user_competencias")
     */
    public function competencias(Request $request)
    {
        // $user = $this->getUser();
        // $username = $user->getUsername();
        // replace this example code with whatever you need
        return $this->render('user/competencias.html.twig');
    }

    /**
     * @Route("/ejc", name="front_ejc", defaults={"pagina_id" = 2} )
     */
    public function ejc($pagina_id, Request $request){
        // $user = $this->getUser();
        // $username = $user->getUsername();
        $pagina = $this->getDoctrine()->getRepository(Pagina::class)->find($pagina_id);
        if(!empty($pagina)){
            return $this->render('paginas/mostrar-pagina.html.twig', array('pagina' => $pagina));
        }else{
            // replace this example code with whatever you need
            return $this->render('front/ejc.html.twig');
        }
        
    }

    /**
     * @Route("/metodologia", name="front_metodologia", defaults={"pagina_id" = 3})
     */
    public function metodologia($pagina_id, Request $request){
        // $user = $this->getUser();
        // $username = $user->getUsername();
        // replace this example code with whatever you need
        $pagina = $this->getDoctrine()->getRepository(Pagina::class)->find($pagina_id);
        if(!empty($pagina)){
            return $this->render('paginas/mostrar-pagina.html.twig', array('pagina' => $pagina));
        }else{
            // replace this example code with whatever you need
            return $this->render('front/metodologia.html.twig');
        }
    }
}
