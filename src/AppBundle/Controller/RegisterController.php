<?php
// src/Controller/RegistrationController.php
namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Form\ParticipanteType;
use AppBundle\Form\AdminType;
use AppBundle\Entity\User;
use AppBundle\Entity\Participante;
use AppBundle\Entity\Admin;
use AppBundle\Entity\Tutor;
use AppBundle\Form\TutorType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends Controller
{
    /**
     * @Route("/registro-especialista", name="user_registration")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // 1) build the form
        $admin = new Admin();

        
        $form = $this->createForm(AdminType::class, $admin);
        $message =  null;

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $user = $admin->getUser();
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            

            $user->setRoles(array('ROLE_ADMIN'));
            $user->setAdmin($admin);
            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->persist($admin);
            $entityManager->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            // return $this->redirectToRoute('reto_list');
            $message = 'Registro Exitoso!';
            // return $this->redirectToRoute('user_registration2', array('user_id' => $user->id));


        }
   
        return $this->render(
            'registration/admin-register.html.twig',
            array('form' => $form->createView(), 'message' => $message)
        );
    }


    /**
     * @Route("/registro-participante", name="user_participante")
     */
    public function participante(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $participante = new Participante();

        $form = $this->createForm(ParticipanteType::class, $participante);
        $form->handleRequest($request);
        $message = null;
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $user = $participante->getUser();
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            //llenar
            
            // $useremail = $form->get('username')->getData();
            // $parts = explode("@", $useremail);
            // $username = $parts[0];
            // $user->setUsername($username);
            $user->setRoles(array('ROLE_USER'));
            $user->setParticipante($participante);

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->persist($participante);
            $entityManager->flush();

            // return $this->redirectToRoute('reto_list');
            $message = 'Registro Exitoso!';
           // return $this->redirectToRoute('user_registration2', array('user_id' => $user->id));
        }
        return $this->render(
            'registration/register.html.twig',
            array('form' => $form->createView(), 'message' => $message)
        );
    }

    /**
     * @Route("/registro-tutor", name="user_registration3")
     */
    public function register3(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // 1) build the form
        $tutor = new Tutor();

        
        $form = $this->createForm(TutorType::class, $tutor);
        $message =  null;

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $user = $tutor->getUser();
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            

            $user->setRoles(array('ROLE_TUTOR'));
            $user->setTutor($tutor);
            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->persist($tutor);
            $entityManager->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            // return $this->redirectToRoute('reto_list');
            $message = 'Registro Exitoso!';
            // return $this->redirectToRoute('user_registration2', array('user_id' => $user->id));


        }
   
        return $this->render(
            'registration/tutor-register.html.twig',
            array('form' => $form->createView(), 'message' => $message)
        );
    }
}
