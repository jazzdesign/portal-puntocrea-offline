<?php

namespace AppBundle\Controller;

  use AppBundle\Entity\CBV;
  use AppBundle\Entity\CNB;
  use AppBundle\Entity\EJC;
  use AppBundle\Entity\competodas;
  use Symfony\Component\HttpFoundation\Response;
  use Symfony\Component\HttpFoundation\Request;

  use AppBundle\Form\EjcType;
  
  use Symfony\Component\Routing\Annotation\Route;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;

  use Symfony\Component\Form\Extension\Core\Type\TextType;
  use Symfony\Component\Form\Extension\Core\Type\TextareaType;
  use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class TablesController extends Controller
{
    /**
     * @Route("/competencias-cnb", name="cnb_list")
     * @Method({"GET"})
     */

    public function cnbList(){
      $cnbs = $this->getDoctrine()->getRepository(CNB::class)->findAll();
      return $this->render('tables/list-cnb.html.twig', array('cnbs' => $cnbs));
    }

    /**
     * @Route("/competencias-cbv", name="cbv_list")
     * @Method({"GET"})
     */

    public function cbvList(){
      $cbvs = $this->getDoctrine()->getRepository(CBV::class)->findAll();
      return $this->render('tables/list-cbv.html.twig', array('cbvs' => $cbvs));
    }


    /**
     * @Route("/competencias-todas", name="competencias_list")
     * @Method({"GET"})
     */

    public function competenciasList(){
      $cbvs = $this->getDoctrine()->getRepository(competodas::class)->findAll();
      return $this->render('tables/list-cbv.html.twig', array('cbvs' => $cbvs));
    }

    /**
     * @Route("/competencias-ejc", name="ejc_list")
     * @Method({"GET", "POST"})
     */

    public function ejcList(Request $request){
      $message = null;
      $ejcs = $this->getDoctrine()->getRepository(EJC::class)->findAll();
      $ejc = new EJC();

      $form = $this->createForm(EjcType::class, $ejc);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ejc);
            $entityManager->flush();

            return $this->redirectToRoute('ejc_list');
            $message = 'Competencia Ingresada!';
            // return $this->redirectToRoute('user_registration2', array('user_id' => $user->id));


      }

      return $this->render('tables/list-ejc.html.twig', array('form' => $form->createView(), 'message' => $message, 'ejcs' => $ejcs));
    }

    /**
     * @Route("/competencias-ejc/edit/{id}", name="edit_competencia_ejc")
     * @Method({"GET", "POST"})
     */

    public function ejcEdit(Request $request, $id){
      $message = null;
      $ejc = new EJC();
      $ejc = $this->getDoctrine()->getRepository(EJC::class)->find($id);

      $form = $this->createForm(EjcType::class, $ejc);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            // $entityManager->persist($ejc);
            $entityManager->flush();

            return $this->redirectToRoute('ejc_list');
            $message = 'Competencia Ingresada!';
            // return $this->redirectToRoute('user_registration2', array('user_id' => $user->id));


      }

      return $this->render('tables/edit-ejc.html.twig', array('form' => $form->createView(), 'message' => $message));
    }

    /**
     * @Route("/competencias-ejc/borrar/{id}", name="delete_competencia_ejc")
     */
      public function deleteAspecto(Request $request, $id){
        $ejc = $this->getDoctrine()->getRepository(EJC::class)->find($id);

        $entityManager = $this->getDoctrine()->getManager();
          $entityManager->remove($ejc);
          $entityManager->flush();

          // $response = new Response();
          // $response->send();
          return $this->redirectToRoute('ejc_list');
      }

}
