<?php
// src/Form/RubricaType.php
namespace AppBundle\Form;

use AppBundle\Entity\Rubrica;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class RubricaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descripcion', TextType::class, array(
              'label' => 'Descripción',
              'attr' => array('class' => 'form-control')))
            ->add('descripcioncorta', TextType::class, array(
              'label' => 'Descripción Corta',
              'attr' => array('class' => 'form-control')))
            ->add('nologro', TextType::class, array(
              'label' => 'No Logro',
              'attr' => array('class' => 'form-control')))
            ->add('parciallogro', TextType::class, array(
              'label' => 'Logro Parcial',
              'attr' => array('class' => 'form-control')))
            ->add('totallogro', TextType::class, array(
              'label' => 'Logro Total',
              'attr' => array('class' => 'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Rubrica::class,
        ));
    }
}