<?php
// src/Form/TutorType.php
namespace AppBundle\Form;

use AppBundle\Entity\Tutor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class TutorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', UserType::class, array(
              'label' => 'Datos de Usuario',
              'attr' => array('class' => 'form-control')))
            ->add('cui', IntegerType::class, array(
              'label' => 'Número CUI',
              'attr' => array('class' => 'form-control')))
            ->add('nombres', TextType::class, array(
              'label' => 'Nombres',
              'attr' => array('class' => 'form-control')))
            ->add('apellidos', TextType::class, array(
              'label' => 'Apellidos',
              'attr' => array('class' => 'form-control')))
            ->add('fechanacimiento', BirthdayType::class, array(
              'label' => 'Fecha de Nacimiento',
              'years' => range(date('Y') -0, date('Y') -100),
              'attr' => array('class' => 'form-control'),
              'placeholder' => array(
                  'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
              )))
            ->add('genero', ChoiceType::class, array(
              'label' => 'Género',
              'attr' => array('class' => 'form-control'),
              'choices'  => array(
                  'Femenino' => 'F',
                  'Masculino' => 'M',
                  ),
              ))
            ->add('telefonocasa', IntegerType::class, array(
              'label' => 'Teléfono de Casa',
              'attr' => array('class' => 'form-control')))
            ->add('telefonocelular', IntegerType::class, array(
              'label' => 'Teléfono Celular',
              'attr' => array('class' => 'form-control', 'type' => 'number')))
            ->add('comunidad', TextType::class, array(
              'label' => 'Nombre de tu Comunidad',
              'attr' => array('class' => 'form-control')))
            ->add('municipio', TextType::class, array(
              'label' => 'Nombre de tu Municipio',
              'attr' => array('class' => 'form-control')))
            ->add('departamento', TextType::class, array(
              'label' => 'Nombre de tu Departamento',
              'attr' => array('class' => 'form-control')))
            ->add('direccionfisica', TextType::class, array(
              'label' => 'Direccion Física',
              'attr' => array('class' => 'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Tutor::class,
        ));
    }
}