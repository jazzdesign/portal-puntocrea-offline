<?php
// src/Form/EjcType.php
namespace AppBundle\Form;

use AppBundle\Entity\EJC;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class EjcType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categoria', ChoiceType::class, array(
                  'label' => 'Categoría STEAM',
                  'attr' => array('class' => 'form-control'),
                  'choices'  => array(
                      'Ciencia' => 'S',
                      'Tecnología' => 'T',
                      'Ingeniería' => 'E',
                      'Arte' => 'A',
                      'Matemática' => 'T',
                      ),
                  ))
            ->add('fuente', TextareaType::class, array(
              'label' => 'Fuente',
              'attr' => array('class' => 'form-control')))
            ->add('competencia', TextareaType::class, array(
              'label' => 'Competencia',
              'attr' => array('class' => 'form-control')))
            ->add('indicadorlogro', TextareaType::class, array(
              'label' => 'Indicador de Logro',
              'attr' => array('class' => 'form-control')))
            ->add('contenido', TextareaType::class, array(
              'label' => 'Contenido',
              'attr' => array('class' => 'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => EJC::class,
        ));
    }
}